FROM ghcr.io/cirruslabs/flutter as builder
RUN sudo apt update && sudo apt install curl -y

RUN git clone https://gitlab.com/famedly/fluffychat.git
RUN mv fluffychat app
#COPY . /app
WORKDIR /app
COPY config.sample.json .
RUN ./scripts/prepare-web.sh
ADD build-web.sh ./scripts/.
RUN ./scripts/build-web.sh

FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df
RUN mkdir -p /app/code/html/
COPY --from=builder /app/build/web /app/code/html/

RUN yarn global add http-server

COPY start.sh /app/code/
COPY index.html /app/code/html
COPY config.sample.json /app/code/html/config.json

CMD [ "/app/code/start.sh" ]
