#!/bin/bash

set -eu

mkdir -p /app/data/
cp -aR /app/code/html/* /app/data/

echo "==> Starting FluffyChat"
exec /usr/local/bin/gosu cloudron:cloudron http-server /app/data -a 0.0.0.0 --port 3000 -d false
